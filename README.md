# Hungry Island
Pokémon GO like game centered around simplified gamedesign for mentally handicapped people.

We got tasked to do this for 'S Heeren Loo's Nursing Home for the mentally handicapped.

The [game](https://drive.google.com/drive/folders/1tBpwPyHOWXX3NkQKpbk8UkJX6wu2J-Sq)

# Work processes
We started off the project with everyone brainstorming over what to do. Eventually we narrowed it down to 4 ideas that we presented to the client that we then asked for feedback. Then we got to a decision and discussing who would take what task, and show it in the Trello board. After that, we got to work on it. After we started we did run into some issues that we resolved pretty easily by simply asking someone else if he may know what issue we ran into. Furthermore, we also did regular code reviews with each other right before a pull request, so that everyone's code used the right code conventions and was of the standard that we were aiming for. Instead of GitHub, we used BitBucket as a way to use an alternative and to see if it was something we could use in the real project. All the devs concluded that BitBucket was easier to use, and more appealing in general.

# Team
This project was made in a team of 8 people, 4 devs and 4 artists.
Devs:
Floris van den Berg (Lead dev).
Bradley Bekker.
Tiësto Schouten.
Brandon Ruigrok.

Artists:
Jaynie Beijner (Lead artist).
Calvin Buth.
Quincy Sung.
Roan van Ligten.

# Sources
In the project we've worked with 3 plugins, each being different from each other.
1. [Pedometer by NatSuite](https://github.com/natsuite/NatStep/tree/1.1-builds)
2. [LeanTween by Dented Pixel](https://assetstore.unity.com/packages/tools/animation/leantween-3595)
3. Text mesh Pro by Unity Technologies

# Software
For the development of the project we've used a few different programms, all named here.
1. Unity3D Engine 2020.2.4f1
2. Trello
3. Sourcetree
4. Bitbucket