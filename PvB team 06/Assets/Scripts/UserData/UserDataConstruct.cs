using System;

namespace PvB.ProjectName.Data
{
    /// <summary>
    /// The actual data struct for UserData
    /// </summary>
    [Serializable]
    public class UserDataConstruct
    {
        /// <summary>
        /// coins you can use for upgrading your town
        /// </summary>
        public int Coins = 0;
        /// <summary>
        /// How many people you have
        /// </summary>
        public int People = 7;
        /// <summary>
        /// Bonus you can also get. only used for flexing on your friends
        /// </summary>
        public int Crystals = 0;
        /// <summary>
        /// Gets removed daily gets added  after a nice walk. when you don't have enough, people leave
        /// </summary>
        public int Food = 0;
        /// <summary>
        /// The last time the player logged off, used for calculating food loss
        /// </summary>
        public int[] LastDate;
        /// <summary>
        /// The level the town is currently in
        /// </summary>
        public int TownLevel = 1;
    }
}
