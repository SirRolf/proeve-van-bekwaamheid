using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace PvB.ProjectName.Data
{
    /// <summary>
    /// The class that stores all the data and allowes you to update them
    /// </summary>
    public static class UserData
    {
        /// <summary>
        /// The actual data object
        /// </summary>
        private static UserDataConstruct data = new UserDataConstruct();

        /// <summary>
        /// Get's called whenever you update one of the data variables
        /// </summary>
        public static Action Refresh;

        /// <summary>
        /// Used for Adding or retracting food
        /// </summary>
        /// <param name="_amount">the ammount of food added/retracted</param>
        public static void UpdateFood(int _amount)
        {
            data.Food += _amount;
            Save();
            if (Refresh != null)
                Refresh();
        }

        /// <summary>
        /// Returns the food the player has
        /// </summary>
        /// <returns>the food the player has</returns>
        public static int GetFood()
        {
            return data.Food;
        }

        /// <summary>
        /// Used for Adding or retracting crystals
        /// </summary>
        /// <param name="_amount">the ammount of crystals added/retracted</param>
        public static void UpdateCrystals(int _amount)
        {
            data.Crystals += _amount;
            Save();
            if (Refresh != null)
                Refresh();
        }

        /// <summary>
        /// Returns the crystals the player has
        /// </summary>
        /// <returns>the crystals the player has</returns>
        public static int GetCrystals()
        {
            return data.Crystals;
        }

        /// <summary>
        /// Used for Adding or retracting people
        /// </summary>
        /// <param name="_amount">the ammount of people added/retracted</param>
        public static void UpdatePeople(int _amount)
        {
            data.People += _amount;
            Save();
            if (Refresh != null)
                Refresh();
        }

        /// <summary>
        /// Returns the people the player has
        /// </summary>
        /// <returns>the people the player has</returns>
        public static int GetPeople()
        {
            return data.People;
        }

        /// <summary>
        /// Used for Adding or retracting coins
        /// </summary>
        /// <param name="_amount">the ammount of coins added/retracted</param>
        public static void UpdateCoins(int _amount)
        {
            data.Coins += _amount;
            Save();
            if (Refresh != null)
                Refresh();
        }

        /// <summary>
        /// Returns the coins the player has
        /// </summary>
        /// <returns>the coins the player has</returns>
        public static int GetCoins()
        {
            return data.Coins;
        }

        /// <summary>
        /// Used for changing the last date of the last time the player was logged in
        /// </summary>
        /// <param name="_amount">the last time the player was logged in</param>
        public static void UpdateDate(DateTime _amount)
        {
            int[] value = new int[3];
            value[0] = _amount.Year;
            value[1] = _amount.Month;
            value[2] = _amount.Day;
            data.LastDate = value;
            Save();
            if (Refresh != null)
                Refresh();
        }

        /// <summary>
        /// Returns the last date the player logged out
        /// </summary>
        /// <returns>the last date the player logged out</returns>
        public static DateTime GetDate()
        {
            DateTime date = new DateTime(data.LastDate[0], data.LastDate[1], data.LastDate[2]);
            return date;
        }

        /// <summary>
        /// Update the town to a certail level. everything above 3 will automatically be set to 3
        /// </summary>
        /// <param name="_amount">the new level</param>
        public static void UpdateTownLevel(int _amount)
        {
            if (_amount > 3)
                _amount = 3;
            data.TownLevel = _amount;
            Save();
            if (Refresh != null)
                Refresh();
        }

        /// <summary>
        /// Returns the TownLevel the player is at
        /// </summary>
        /// <returns>the TownLevel the player is at</returns>
        public static int GetTownLevel()
        {
            return data.TownLevel;
        }

        /// <summary>
        /// Saves the data locally
        /// </summary>
        private static void Save()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/UserData.bin", FileMode.Create);
            formatter.Serialize(stream, data);
            stream.Close();
        }

        /// <summary>
        /// loads the data from a local location
        /// </summary>
        public static void Load()
        {
            if (!File.Exists(Application.persistentDataPath + "/UserData.bin"))
            {
                Debug.LogWarning("No data in Data Path: " + Application.persistentDataPath + "/UserData.bin. Making a new one");

                BinaryFormatter createFormatter = new BinaryFormatter();
                FileStream createStream = new FileStream(Application.persistentDataPath + "/UserData.bin", FileMode.Create);

                DateTime currentDate = DateTime.Now;

                int[] value = new int[3];
                value[0] = currentDate.Year;
                value[1] = currentDate.Month;
                value[2] = currentDate.Day;
                data.LastDate = value;

                createFormatter.Serialize(createStream, data);
                createStream.Close();
                return;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/UserData.bin", FileMode.Open);

            data = formatter.Deserialize(stream) as UserDataConstruct;
            stream.Close();
        }

        public static void DeleteSave()
        {
            File.Delete(Application.persistentDataPath + "/UserData.bin");
            data = new UserDataConstruct();
            Load();
        }
    }
}

