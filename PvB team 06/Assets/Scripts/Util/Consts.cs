using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.ProjectName.Util
{
	/// <summary>
	/// class filled with all kinds of consts that could come in handy
	/// </summary>
    public static class Consts
    {
		public const int TOWN_COST_BASE = 50;
		public const int TOWN_COST_MULTIPLIER = 10;
		public const int PEOPLE_COST_BASE = 1;
		public const int PEOPLE_COST_MULTIPLIER = 2;
	}
}
