using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.ProjectName.Events
{
	/// <summary>
	/// Class used for storring a reward in a event
	/// </summary>
	public class Reward
	{
		/// <summary>
		/// Amount of coins the player gets
		/// </summary>
		public int Coins { get; private set; }
		/// <summary>
		/// Amount of food the player gets
		/// </summary>
		public int Food { get; private set; }
		/// <summary>
		/// Amount of crystals the player gets
		/// </summary>
		public int Crystals { get; private set; }

		/// <summary>
		/// Class used for storring a reward in a event
		/// </summary>
		/// <param name="_coins">Amount of coins the player gets</param>
		/// <param name="_food">Amount of food the player gets</param>
		/// <param name="_crystals">Amount of crystals the player gets</param>
		public Reward(int _coins, int _food, int _crystals)
		{
			Coins = _coins;
			Food = _food;
			Crystals = _crystals;
		}
	}
}
