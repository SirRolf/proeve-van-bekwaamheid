using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PvB.ProjectName.Events
{
    /// <summary>
    /// Behavior for the food that falls down in the events
    /// </summary>
    public class FallingFoodBehavior : MonoBehaviour
    {
        /// <summary>
        /// Time before the food gets destroyed
        /// </summary>
        private const float TIME = 3;

        /// <summary>
        /// The image of this falling food
        /// </summary>
        [SerializeField]
        private Image myImage;
        /// <summary>
        /// all the possible sprites
        /// </summary>
        [SerializeField]
        private Sprite[] sprites;
        /// <summary>
        /// the rigid body of this sprite
        /// </summary>
        [SerializeField]
        private Rigidbody2D rigidbody;

        /// <summary>
        /// the timer for checking if enough time passed before deleting
        /// </summary>
        private float timer;

        /// <summary>
        /// Init for the first time initializing this
        /// </summary>
        public void Init()
        {
            myImage.sprite = sprites[Random.Range(0,sprites.Length)];
            Vector2 force = new Vector2(Random.Range(-35000, 35000), Random.Range(100000, 130000));
            rigidbody.AddForce(force);
            rigidbody.AddTorque(Random.Range(-500, 500));
            LeanTween.scale(gameObject, new Vector3(Random.Range(0.8f, 1.2f), Random.Range(0.8f, 1.2f), 1f), 1f)
                .setEase(LeanTweenType.easeInCubic);
        }

        /// <summary>
        /// check if the food should be destroyed
        /// </summary>
        private void Update()
        {
            timer += Time.deltaTime;
            if (timer > TIME)
                Destroy(gameObject);
        }
    }
}