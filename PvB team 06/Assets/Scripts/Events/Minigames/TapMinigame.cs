using PvB.ProjectName.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PvB.ProjectName.Events
{
    /// <summary>
    /// Minigame where you click on the box to open it
    /// </summary>
    public class TapMinigame : Events
    {
        /// <summary>
        /// The canvas group this event is a part of
        /// </summary>
        [SerializeField]
        private CanvasGroup canvasGroup;
        /// <summary>
        /// The box in the middle of the screen
        /// </summary>
        [SerializeField]
        private Button boxButton;
        /// <summary>
        /// The image of the box in the middle of the screen
        /// </summary>
        [SerializeField]
        private Image boxImage;
        /// <summary>
        /// sprite of a closed box
        /// </summary>
        [SerializeField]
        private Sprite closedBox;
        /// <summary>
        /// sprite of a open box
        /// </summary>
        [SerializeField]
        private Sprite openBox;
        /// <summary>
        /// the coin text component
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI coinText;
        /// <summary>
        /// Falling food preset
        /// </summary>
        [SerializeField]
        private FallingFoodBehavior FallingFood;
        /// <summary>
        /// Canvas this event is a part of
        /// </summary>
        [SerializeField]
        private Transform Canvas;

        /// <summary>
        /// The health of this box
        /// </summary>
        private int boxHealth;

        /// <summary>
        /// Displays this event and gets everything ready
        /// </summary>
        /// <param name="_onComplete">Gets called when this event is over</param>
        public override void Display(Action _onComplete)
        {
            ChangeText();

            LeanTween.alphaCanvas(canvasGroup, 1f, 1f)
                .setEase(LeanTweenType.easeInQuart);
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            boxHealth = UnityEngine.Random.Range(4, 15);
            UserData.Refresh += ChangeText;
            boxButton.onClick.AddListener(BoxHit);
            boxImage.sprite = closedBox;
            int coinReward = Mathf.RoundToInt(UnityEngine.Random.Range(.1f, .2f) * UserData.GetPeople());
            int foodReward = Mathf.RoundToInt(UnityEngine.Random.Range(.5f, 1f) * UserData.GetPeople());
            int crystalReward = Convert.ToInt32(UnityEngine.Random.Range(0f, 1f) > .9f);
            reward = new Reward(coinReward, foodReward, crystalReward);

            onComplete = _onComplete;
        }

        /// <summary>
        /// Hides this event
        /// </summary>
        public override void Hide()
        {
            LeanTween.alphaCanvas(canvasGroup, 0f, 1f)
                .setEase(LeanTweenType.easeInQuart);
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            if (onComplete != null)
                onComplete.Invoke();
        }

        /// <summary>
        /// For when you press the box
        /// </summary>
        private void BoxHit()
        {
            boxHealth--;
            if (boxHealth == 0)
            {
                boxButton.onClick.RemoveAllListeners();
                GiveReward();
                boxImage.sprite = openBox;
                LeanTween.cancel(boxButton.gameObject);
                LeanTween.scale(boxButton.gameObject, new Vector3(1.25f, 1.25f, 1f), .5f)
                    .setEase(LeanTweenType.easeOutBounce)
                    .setOnComplete(Hide);
                for (int i = 0; i < reward.Food; i++)
                {
                    Vector2 position = new Vector2(boxButton.transform.position.x + UnityEngine.Random.Range(-350, 350), boxButton.transform.position.y);
                    Instantiate(FallingFood, position, Quaternion.identity, Canvas).Init();
                }
                return;
            }
            LeanTween.cancel(boxButton.gameObject);
            LeanTween.scale(boxButton.gameObject, new Vector3(1.1f,1.1f,1f), .5f)
                .setEase(LeanTweenType.easeOutCirc)
                .setOnComplete(() => LeanTween.scale(boxButton.gameObject, new Vector3(1f, 1f, 1f), .5f).setEase(LeanTweenType.easeOutCirc));
        }

        /// <summary>
        /// Changes the text in this event
        /// </summary>
        private void ChangeText() => coinText.text = $"{UserData.GetCoins()}x";
    }
}

