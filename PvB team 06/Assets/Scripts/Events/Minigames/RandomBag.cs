using PvB.ProjectName.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PvB.ProjectName.Events
{
    /// <summary>
    /// Minigame where you click on the bag to open it
    /// </summary>
    public class RandomBag : Events
    {
        /// <summary>
        /// The canvas group this event is a part of
        /// </summary>
        [SerializeField]
        private CanvasGroup canvasGroup;
        /// <summary>
        /// The bag in the middle of the screen
        /// </summary>
        [SerializeField]
        private Button bagButton;
        /// <summary>
        /// the coin text component
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI coinText;
        /// <summary>
        /// Canvas this event is a part of
        /// </summary>
        [SerializeField]
        private Transform canvas;
        /// <summary>
        /// Falling food preset
        /// </summary>
        [SerializeField]
        private FallingFoodBehavior fallingFood;
        /// <summary>
        /// Falling food preset
        /// </summary>
        [SerializeField]
        private FallingFoodBehavior fallingCoin;
        /// <summary>
        /// The offset for the falling objects that spawn when you press the bag
        /// </summary>
        [Header("Adjustments")]
        [SerializeField, Tooltip("The offset for the falling objects that spawn when you press the bag")]
        private int fallingObjectOffset;

        /// <summary>
        /// Displays this event and gets everything ready
        /// </summary>
        /// <param name="_onComplete">Gets called when this event is over</param>
        public override void Display(Action _onComplete)
        {
            ChangeText();

            LeanTween.alphaCanvas(canvasGroup, 1f, 1f)
                .setEase(LeanTweenType.easeInQuart);
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            UserData.Refresh += ChangeText;
            bagButton.onClick.AddListener(BagHit);
            int coinReward = Mathf.RoundToInt(UnityEngine.Random.Range(1f, 2f) * UserData.GetPeople());
            int foodReward = Mathf.RoundToInt(UnityEngine.Random.Range(.1f, .2f) * UserData.GetPeople());
            reward = new Reward(coinReward, foodReward, 0);

            onComplete = _onComplete;
        }

        /// <summary>
        /// Displays this event and gets everything ready
        /// </summary>
        /// <param name="_onComplete">Gets called when this event is over</param>
        public override void Hide()
        {
            LeanTween.alphaCanvas(canvasGroup, 0f, 1f)
                .setEase(LeanTweenType.easeInQuart);
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            if (onComplete != null)
                onComplete.Invoke();
        }

        /// <summary>
        /// For when you press the bag
        /// </summary>
        private void BagHit()
        {
            bagButton.onClick.RemoveAllListeners();
            GiveReward();
            LeanTween.scale(bagButton.gameObject,new Vector3(.75f, 1.1f, 1f), .4f)
                .setEase(LeanTweenType.easeOutCirc)
                .setOnComplete(() => LeanTween.scale(bagButton.gameObject, Vector3.one, .5f).setEase(LeanTweenType.easeInCirc));
            for (int i = 0; i < reward.Food; i++)
            {
                Vector2 position = new Vector2(bagButton.transform.position.x + UnityEngine.Random.Range(-35, 35), bagButton.transform.position.y + fallingObjectOffset);
                Instantiate(fallingFood, position, Quaternion.identity, canvas).Init();
            }
            for (int i = 0; i < reward.Coins; i++)
            {
                Vector2 position = new Vector2(bagButton.transform.position.x + UnityEngine.Random.Range(-35, 35), bagButton.transform.position.y + fallingObjectOffset);
                Instantiate(fallingCoin, position, Quaternion.identity, canvas).Init();
            }
            Hide();
        }

        /// <summary>
        /// Changes the text in this event
        /// </summary>
        private void ChangeText()
        {
            coinText.text = $"{UserData.GetCoins()}x";
        }
    }
}
