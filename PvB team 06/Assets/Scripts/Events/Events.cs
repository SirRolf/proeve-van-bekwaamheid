using PvB.ProjectName.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.ProjectName.Events
{
	/// <summary>
	/// abstract class for making new events
	/// </summary>
	public abstract class Events : MonoBehaviour
	{
		/// <summary>
		/// the rewards you get for 
		/// </summary>
		public Reward reward;

		/// <summary>
		/// Gets called whenever this event is done
		/// </summary>
		public Action onComplete;

		/// <summary>
		/// Displays this event and gets everything ready
		/// </summary>
		/// <param name="_onComplete">Gets called when this event is over</param>
		public abstract void Display(Action _onComplete);

		/// <summary>
		/// Hides this event
		/// </summary>
		public abstract void Hide();

		/// <summary>
		/// Adds all the rewards to the UserData
		/// </summary>
		public void GiveReward()
		{
			UserData.UpdateFood(reward.Food);
			UserData.UpdateCoins(reward.Coins);
		}
	}
}
