namespace PvB.ProjectName.Date
{
    using PvB.ProjectName.FoodConsumption;
    using System;
    using UnityEngine;
    using PvB.ProjectName.Data;

    /// <summary>
    /// handles calculation related to when the game was last started
    /// </summary>
    public static class DateLog
    {
        /// <summary>
        /// calculates how many days have passed since the last login
        /// </summary>
        public static void DateUpdateOnLogin()
        {
            DateTime currentDate;
            currentDate = DateTime.Now;
            int daysPastLastLogin;

            TimeSpan differenceInDate = currentDate - UserData.GetDate();
            daysPastLastLogin = (int)differenceInDate.TotalDays;
            UserData.UpdateDate(DateTime.Now);

            FoodConsumption.ConsumeFood(daysPastLastLogin);
        }
    }

}
