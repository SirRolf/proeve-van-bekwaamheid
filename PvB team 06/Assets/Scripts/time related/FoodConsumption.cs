using PvB.ProjectName.Data;

namespace PvB.ProjectName.FoodConsumption
{

    /// <summary>
    /// subtracts an amount of food on a daily basis
    /// </summary>
    public class FoodConsumption
    {
        /// <summary>
        /// the amount of food to consume per day
        /// </summary>
        private static int amountFoodToConsumePerDay = 300;

        /// <summary>
        /// subtracts amountFoodToConsumePerDay multiplied by amountOfDays from UserData.food
        /// </summary>
        /// <param name="amountOfDays">the omount of days worth of food to consume</param>
        public static void ConsumeFood(int amountOfDays)
        {
            int foodToRemove = (amountFoodToConsumePerDay * amountOfDays) * -1;
            UserData.UpdateFood(foodToRemove);
        }
    }
}
