using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using PvB.ProjectName.StepManager;
using PvB.ProjectName.Events;
using PvB.ProjectName.TownOverlay;
using PvB.ProjectName.Data;

//Pair programmed by;
//Brandon Ruigrok & Floris van den Berg
namespace PvB.Projectname.WalkingOverlay
{
    /// <summary>
    /// This class will take care of all the overlay elements when you're walking.
    /// </summary>
    public class WalkingOverlay : MonoBehaviour
    {
        /// <summary>
        /// List for all the minigames
        /// </summary>
        [SerializeField]
        private List<Events> minigames;
        /// <summary>
        /// List for potential random rewards on the way to the goal
        /// </summary>
        [SerializeField]
        private List<Events> randomRewards;

        /// <summary>
        /// 
        /// </summary>
        [SerializeField]
        private List<Sprite> levelOneBoat;
        /// <summary>
        /// 
        /// </summary>
        [SerializeField]
        private List<Sprite> levelTwoBoat;
        /// <summary>
        /// 
        /// </summary>
        [SerializeField]
        private List<Sprite> levelThreeBoat;

        /// <summary>
        /// Component for the TownOverlay script.
        /// </summary>
        [SerializeField]
        private TownOverlay townOverlay;
        /// <summary>
        /// Integer for the amount of steps you need to take
        /// </summary>
        private int stepGoal;
        /// <summary>
        /// The amount of steps you took
        /// </summary>
        private int steps;
        /// <summary>
        /// Tries to stay equal to steps, so that it can randomly generate a reward.
        /// </summary>
        private int previousSteps = 0;
        /// <summary>
        /// Is the main display for the walking overlay.
        /// </summary>
        [SerializeField]
        private CanvasGroup canvasGroup;
        /// <summary>
        /// Is the text for the amount of distance you've gotta walk.
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI distanceGoal;
        /// <summary>
        /// The image of the boat
        /// </summary>
        [SerializeField]
        private Image boat;
        /// <summary>
        /// text where you see how many coins you have
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI coinText;

        /// <summary>
        /// The bar used for showwing how far you are
        /// </summary>
        [SerializeField]
        private GameObject walkingProgressBar;

        /// <summary>
        /// The minimum amount of steps for the step goal
        /// </summary>
        [Header("Adjustments")]
        [SerializeField]
        private int stepMin;
        /// <summary>
        /// The maximum amount of steps for the step goal
        /// </summary>
        [SerializeField]
        private int stepMax;

        /// <summary>
        /// The chance you get a random reward
        /// </summary>
        [SerializeField]
        private float RandomRewardChance;

        /// <summary>
        /// If you are returning from your journey.
        /// </summary>
        private bool travelBack;

        /// <summary>
        /// All the visual elements in one place.
        /// </summary>
        public void Display()
        {
            UpdateCoins();

            stepGoal = UnityEngine.Random.Range(stepMin, stepMax);      //Bound to be changed after discussion.
            StepManager.TakeStep += StepCheck;
            LeanTween.alphaCanvas(canvasGroup, 1f, 1f)                   //CanvasGroup, Alpha, Duration.
                .setEase(LeanTweenType.easeInQuart);

            //Ik weet dat dit shit is maar  het werkt
            if (UserData.GetTownLevel() == 1)
                boat.sprite = levelOneBoat[Convert.ToInt32(travelBack)];
            if (UserData.GetTownLevel() == 2)
                boat.sprite = levelTwoBoat[Convert.ToInt32(travelBack)];
            if (UserData.GetTownLevel() == 3)
                boat.sprite = levelThreeBoat[Convert.ToInt32(travelBack)];
            distanceGoal.text = $"{stepGoal - steps}";          //Visualises the step goal.
        }
        
        /// <summary>
        /// Cleans screen after reaching your destination
        /// </summary>
        private void Hide()
        {
            StepManager.TakeStep -= StepCheck;
            LeanTween.alphaCanvas(canvasGroup, 0f, 1f)
                .setEase(LeanTweenType.easeInQuart);
        }

        /// <summary>
        /// Checks to see if a step's been made
        /// </summary>
        /// <param name="StepsTook">The amount of steps taken</param>
        private void StepCheck(int _stepsTook)
        {
            steps = _stepsTook;
            if (stepGoal - steps <= 0)
                distanceGoal.text = $"0";
            else
                distanceGoal.text = $"{stepGoal - steps}";

            if (steps / stepGoal > 1)
                LeanTween.scaleX(walkingProgressBar, 1f, 1.5f);
            else
                LeanTween.scaleX(walkingProgressBar, steps / stepGoal, 1.5f);

            // Is to activate random chance for running into random reward
            if (previousSteps != steps)
            {
                float rewardChance = UnityEngine.Random.Range(1, 100);
                if (rewardChance <= RandomRewardChance)
                    OpenRandomReward();
                previousSteps = steps;
            }

            if (steps >= stepGoal && !travelBack)
            {
                Hide();
                steps = 0;
                travelBack = true;
                OpenMinigame();
            }

            if(steps >= stepGoal && travelBack)
            {
                Hide();
                steps = 0;
                travelBack = false;
                ReturnToTown();
            }
        }
 
        /// <summary>
        /// Picks random minigame and shows it
        /// </summary>
        private void OpenMinigame() => minigames[UnityEngine.Random.Range(0, minigames.Count - 1)].Display(() => RestartWalkCycle());

        /// <summary>
        /// x Amount of chance for finding a reward per step
        /// </summary>
        private void OpenRandomReward() => randomRewards[UnityEngine.Random.Range(0, randomRewards.Count - 1)].Display(() => RestartWalkCycle());

        /// <summary>
        /// Refreshes walking cycle so you can return.
        /// </summary>
        private void RestartWalkCycle()
        {
            UpdateCoins();
            if (UserData.GetTownLevel() == 1)
                boat.sprite = levelOneBoat[Convert.ToInt32(travelBack)];
            if (UserData.GetTownLevel() == 2)
                boat.sprite = levelTwoBoat[Convert.ToInt32(travelBack)];
            if (UserData.GetTownLevel() == 3)
                boat.sprite = levelThreeBoat[Convert.ToInt32(travelBack)];
            distanceGoal.text = $"{stepGoal - steps}";
            StepManager.TakeStep += StepCheck;
            LeanTween.alphaCanvas(canvasGroup, 1f, 1f).setEase(LeanTweenType.easeInQuart);
        }

        /// <summary>
        /// Returns the player back to the town.
        /// </summary>
        private void ReturnToTown()
        {
            Hide();
            townOverlay.Display();
        }

        private void UpdateCoins()
        {
            coinText.text = $"{UserData.GetCoins()}";
        }
    }
}
