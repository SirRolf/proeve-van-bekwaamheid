using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.ProjectName.Data;
using TMPro;
using PvB.ProjectName.Util;

namespace PvB.ProjectName.TownOverlay
{
	/// <summary>
	/// People managing component
	/// </summary>
	public class TownPeopleComponent : TownComponent
	{
		/// <summary>
		/// Gets the number of people left in the town
		/// </summary>
		[SerializeField]
		private TextMeshProUGUI townPeople;
		/// <summary>
		/// The price of a new person
		/// </summary>
		[SerializeField]
		private TextMeshProUGUI townPeoplePrice;

		/// <summary>
		/// Refreshes when the user data changes and depending on what component
		/// </summary>
		public override void OnRefresh()
		{
			townPeople.text = $"{UserData.GetPeople()}";
			townPeoplePrice.text = $"{Consts.PEOPLE_COST_BASE * (Consts.PEOPLE_COST_MULTIPLIER * UserData.GetPeople() - 1)}";
		}
	}
}
