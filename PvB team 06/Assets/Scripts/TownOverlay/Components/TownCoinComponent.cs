using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.ProjectName.Data;
using TMPro;

namespace PvB.ProjectName.TownOverlay
{
	/// <summary>
	/// Coin management component
	/// </summary>
	public class TownCoinComponent : TownComponent
	{
		/// <summary>
		/// Text gameobject, where the coin text is put into.
		/// </summary>
		[SerializeField]
		private TextMeshProUGUI townCoins;
		
		/// <summary>
		/// Refreshes when the user data changes and depending on what component
		/// </summary>
		public override void OnRefresh() => townCoins.text = $"{UserData.GetCoins()}";
	}
}