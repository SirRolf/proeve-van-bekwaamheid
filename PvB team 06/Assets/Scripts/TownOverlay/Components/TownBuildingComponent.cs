using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PvB.ProjectName.Data;
using TMPro;
using PvB.ProjectName.Util;

namespace PvB.ProjectName.TownOverlay 
{
	/// <summary>
	/// Town managing component
	/// </summary>
    public class TownBuildingComponent : TownComponent
    {
		/// <summary>
		/// Keeps track of the amount of sprites that are used for the town/island
		/// </summary>
		[SerializeField]
        private RectTransform[] upgrades;
		/// <summary>
		/// Keeps track of the amount of sprites that are used for the town/island
		/// </summary>
		[SerializeField]
		private TextMeshProUGUI levelText;
		/// <summary>
		/// The price of a town upgrade
		/// </summary>
		[SerializeField]
		private TextMeshProUGUI townUpgradePrice;

		/// <summary>
		/// Refreshes when the user data changes and depending on what component
		/// </summary>
		public override void OnRefresh()
		{
			int TownLevel = UserData.GetTownLevel();
			for (int i = 0; i < TownLevel; i++)
			{
				levelText.text = $"{TownLevel}";
				LeanTween.alpha(upgrades[i], 1f, 1f);
			}
			townUpgradePrice.text = $"{Consts.TOWN_COST_BASE * (Consts.TOWN_COST_MULTIPLIER * UserData.GetTownLevel() - 1)}";
		}
    }
}