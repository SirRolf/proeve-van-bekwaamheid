using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.ProjectName.Data;
using TMPro;

namespace PvB.ProjectName.TownOverlay
{
	/// <summary>
	/// Food management component
	/// </summary>
	public class TownFoodComponent : TownComponent
	{
		/// <summary>
		/// Gets the amount of food left in the town
		/// </summary>
		[SerializeField]
		private TextMeshProUGUI foodText;
		/// <summary>
		/// The bar for showwing how much food you have
		/// </summary>
		[SerializeField]
		private GameObject foodProgressBar;

		/// <summary>
		/// Refreshes when the user data changes and depending on what component
		/// </summary>
		public override void OnRefresh()
		{
			foodText.text = $"{UserData.GetFood()}/{UserData.GetPeople()}";
			if (UserData.GetFood() / UserData.GetPeople() > 1)
				LeanTween.scaleX(foodProgressBar, 1f, 1.5f);
			else
				LeanTween.scaleX(foodProgressBar, UserData.GetFood() / UserData.GetPeople(), 1.5f);
		}
	}
}