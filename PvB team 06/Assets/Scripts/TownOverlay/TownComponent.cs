using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.ProjectName.TownOverlay
{	
	/// <summary>
	/// Abstract class for town components
	/// </summary>
	public abstract class TownComponent : MonoBehaviour
	{
		/// <summary>
		/// Action that works with the OnRefresh function within this class
		/// </summary>
		public Action Refresh;

		/// <summary>
		/// Abstract method that fills in the changes for this class
		/// </summary>
		public abstract void OnRefresh();

		private void OnEnable() => Refresh += OnRefresh;
	}
}