using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.ProjectName.Data;
using PvB.Projectname.WalkingOverlay;
using UnityEngine.UI;
using PvB.ProjectName.Util;

namespace PvB.ProjectName.TownOverlay
{
	/// <summary>
	/// The main manager class for the town overlay and its components
	/// </summary>
	public class TownOverlay : MonoBehaviour
	{
		/// <summary>
		/// The canvasgroup for this object
		/// </summary>
		[SerializeField]
		private CanvasGroup canvasGroup;
		/// <summary>
		/// The different components that change when refresh happens
		/// </summary>
		[SerializeField]
		private TownComponent[] components;
		/// <summary>
		/// Button to upgrade the town
		/// </summary>
		[SerializeField]
		private Button upgradeButton;
		/// <summary>
		/// Button to buy new people
		/// </summary>
		[SerializeField]
		private Button peopleButton;
		/// <summary>
		/// Button to switch to walk screen
		/// </summary>
		[SerializeField]
		private Button walkButton;
		/// <summary>
		/// reference to the walking overlay
		/// </summary>
		[SerializeField]
		private WalkingOverlay walkingOverlay;

		///<summary>
		///The display function makes the TownOverlay show up/reverts the "changes" that the hide functon made after running.
		///</Summary>
		public void Display()
		{
			LeanTween.alphaCanvas(canvasGroup, 1f, 1f)
				.setEase(LeanTweenType.easeInQuart);
			canvasGroup.blocksRaycasts = true;
			canvasGroup.interactable = true;

			upgradeButton.onClick.AddListener(() => UpdateTown());
			peopleButton.onClick.AddListener(() => BuyPeople());
			walkButton.onClick.AddListener(() => ToWalkOverlay());

			for (int i = 0; i < components.Length; i++)
			{
				UserData.Refresh += components[i].Refresh;
				Debug.Log(components[i].gameObject);
				components[i].Refresh();
			}
		}
		///<summary>
		///The Hide function makes sure the TownOverlay doesn't become interactable when an another Overlay comes over it
		///</summary>
		public void Hide()
		{
			LeanTween.alphaCanvas(canvasGroup, 0f, 1f)
			  .setEase(LeanTweenType.easeInQuart);
			canvasGroup.blocksRaycasts = false;
			canvasGroup.interactable = false;

			for (int i = 0; i < components.Length; i++)
				UserData.Refresh -= components[i].Refresh;
		}
		///<summary>
		///The Update town function is there to make it possible to press a button and then increase the size of the town
		///</summary>
		private void UpdateTown()
		{
			if (UserData.GetCoins() >= Consts.TOWN_COST_BASE * (Consts.TOWN_COST_MULTIPLIER * UserData.GetTownLevel() -1))
			{
				UserData.UpdateTownLevel(UserData.GetTownLevel() + 1);
				UserData.UpdateCoins(-(Consts.TOWN_COST_BASE * (Consts.TOWN_COST_MULTIPLIER * UserData.GetTownLevel() - 1)));
			}
			else
				Debug.LogWarning("Not enough money for a townn upgrade");
		}

		/// <summary>
		/// Allowes you to buy people
		/// </summary>
		private void BuyPeople()
		{
			if (UserData.GetCoins() >= Consts.PEOPLE_COST_BASE * (Consts.PEOPLE_COST_MULTIPLIER * UserData.GetPeople() - 1))
			{
				UserData.UpdatePeople(1);
				UserData.UpdateCoins(-(Consts.PEOPLE_COST_BASE * (Consts.PEOPLE_COST_MULTIPLIER * UserData.GetPeople() - 1)));
			}
			else
				Debug.LogWarning("Not enough money for new people");
		}
		
		///<summary>
		///When the ToWalkOverlay function gets called it will hide the TownOverlay and then will make the WalkingOverlay appear over it
		///</summary>
		private void ToWalkOverlay()
		{
			Hide();
			walkingOverlay.Display();
		}
	}
}
