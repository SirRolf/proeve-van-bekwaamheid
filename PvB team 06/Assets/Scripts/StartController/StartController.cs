using PvB.ProjectName.Data;
using PvB.ProjectName.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.ProjectName.TownOverlay;
using PvB.ProjectName.Date;

namespace PvB.ProjectName.StartController
{
    public class StartController : MonoBehaviour
    {
        [SerializeField]
        private TownOverlay.TownOverlay townOverlay;

        private void Awake()
        {
            UserData.Load();
            //UserData.DeleteSave();
            DateLog.DateUpdateOnLogin();
            townOverlay.Display();
        }
    }
}

