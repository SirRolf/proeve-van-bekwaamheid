using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PedometerU;
using System;

namespace PvB.ProjectName.StepManager
{
    /// <summary>
    /// This class makes use of the Opensource pedometer api made by github user "natsuite"
    /// https://github.com/natsuite/NatStep/tree/1.1-builds
    /// handles step taking
    /// </summary>
    public static class StepManager
    {
        /// <summary>
        /// holds the pedometer and is the link to the api
        /// </summary>
        private static Pedometer pedometer = new Pedometer(UpdateStep);

        /// <summary>
        /// this action is called every step contains the steps argument form UpdateStep when triggered calls all contained functions and gives them steps as an argument 
        /// </summary>
        static public Action<int> TakeStep;

        /// <summary>
        /// every time a step is taken UpdateStep will trigger the TakeStep action and all functions contained there in
        /// </summary>
        /// <param name="steps"> Amount of steps taken since app launch </param>
        /// <param name="distance">distance walked in meters since app launch (not a hundred percent accurate)</param>
        static private void UpdateStep(int steps, double distance) => TakeStep(steps);
    }
}
